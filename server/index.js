const path = require('path');
const express =  require('express');
const fs = require('fs')
const https = require('https')
const ffmpeg = require('fluent-ffmpeg')
const app = express();
const IncomingForm = require('formidable').IncomingForm
const cors = require('cors')
const bodyParser = require('body-parser');
const {spawn} = require('child_process');
const graphMaker = require(path.join(__dirname+'/scripts/graphMaker.js'))
const config = require(path.join(__dirname+'/config.js'))
const glob = require('glob')
const zipFolder = require('zip-folder')
const fkill = require('fkill')
const find = require('find-process')
let {PythonShell} = require('python-shell')

console.log(config)
////////////////////////////////////////
//	Event emitter
////////////////////////////////////////
const EventEmitter = require('events');

var myEmitter = new EventEmitter();
myEmitter.setMaxListeners(0)
////////////////////////////////////////
//	OPTIONS FOR THE SERVER
////////////////////////////////////////

//options for the CORS policy
var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
}

//SSL option, in order to set up a HTTPS connection

const keyPath = path.join(__dirname, "security", "cert.key")
const certPath = path.join(__dirname, "security", "cert.pem")
const options =  {
  key: fs.readFileSync(keyPath),
  cert: fs.readFileSync(certPath),
  ca:[
    fs.readFileSync(certPath)
  ]
}
////////////////////////////////////////
//  Prepared bash commands
////////////////////////////////////////
/**
 * @returns A promise which resolves the PID of the process, otherwise rejects with an error
 * @param {*} name Name of the process you want to get the PID
 */
function pidof(name){
  return new Promise((resolve, reject)=>{
    find('name', name)
    .then((list) => {
      resolve(list)
    })
    .catch(err => {
      reject(err)
    })
  })
}

function killProcess(processName) {
  return new Promise((resolve, reject)=>{
    let options = {
      force: true,
      tree: true,
      ignoreCase: true
    }
    if(process.platform=="win32")
      processName+=".exe"

    fkill(processName, options)
    .then(
      resolve()
    )
    .catch((err)=>{
      reject("error while killing a process. Error message:\n"+err)
    })
  })
}

function spawnRTSPServer() {
  return new Promise((resolve, reject)=>{
    console.log("spawning RTSP server...")
    var command = ""
    if (process.platform==="win32"){
      command = "ffmpeg"
    }
    else{
      command = "sudo ffmpeg"
    }
    command += ' -rtsp_flags listen -i rtsp://127.0.0.1:'+config.CLIENTPORT+' -pix_fmt rgb24 -f opengl "video"'
    exec(command, function(err) {
      if (err)
        reject('error while trying to spawn RTSP server'+err)
      else {
      }
    })
    console.log('RTSP server up')
    resolve()
  })
}


function spawnTshark(fileName, _protocol){
  return new Promise((resolve)=>{
    var protocol
    var port
    switch (_protocol) {
      case 'HTTPS':
        protocol= ['-O', 'TLSv1.2']
        port= ['-f', "port "+config.SERVERPORT]
        break;
      case 'RTP':
        protocol= ['-O', 'RTP']
        port= ['-f', "port "+config.CLIENTPORT]
        break;
      case 'SRTP':
        protocol= ['-O', 'RTP']
        port= ['-f', "port "+config.CLIENTPORT]
        break;
      case 'RTSP':
        protocol= ['-O', 'RTSP']
        port= []
      default:
        break;
    }

    let dir = path.join(__dirname, 'results')
    //create the folder if it does not exists
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }

    let command
    let options = []
    if (process.platform==="win32"){
      command = "tshark"
      options.push(...["-i", "Npcap Loopback Adapter"])
    }
    else{
      command = "sudo"
      options.push(["stdbuf", "-o0"])
      options.push(["tshark", "-i", "lo"])
    }
    options.push(...[...protocol, ...port, ...('-l -T fields -E separator=,'+
    ' -E quote=d -E header=y -e frame.len -e _ws.col.Info -e _ws.col.Time -e _ws.col.Source').split(" ")])

    let outputFile = path.join(__dirname,'results','CAP_'+fileName+'.csv');

    let tshark  = spawn(command, options)
    tshark.stdout.on('data', (data)=>{
      fs.appendFile(outputFile, data, function (err){
        if(err)
          console.log("error during writing to file"+err)
      })
    })
    console.log('Sniffing traffic..')
    setTimeout(()=>{
      resolve()
    }, 2000)

  })
}

////////////////////////////////////////
//	ffmpeg call to stream the video
////////////////////////////////////////
function stream(res, fps, crf, fileName, protocol) {


  return new Promise((resolve, reject)=>{

    ////////////////////////////////////////
    //	setting parameters
    ////////////////////////////////////////

    //flag for enabling srtp
    let srtp = false;

    if (fps === undefined)
    fps = 25

    if (crf === undefined)
      crf = 23

    let format
    console.log(protocol)
    switch (protocol) {
      case "HTTPS":
        format = 'mp4'
        res = 'https://127.0.0.1:'+ config.SERVERPORT;
        break;
      case "RTP":
        format = 'rtp'
        res = 'rtp://127.0.0.1:'+ config.CLIENTPORT;
        break;
      case "SRTP":
        format = 'rtp'
        res = 'srtp://127.0.0.1:'+ config.CLIENTPORT;
        srtp = true
        break;
      case "RTSP":
        format = "rtsp"
        res = 'rtsp://127.0.0.1:'+ config.CLIENTPORT;
        break;
      default:
        reject('error: invalid protocol. The protocol is currently not supported.');
    }

    ////////////////////////////////////////
    //	starting the streaming
    ////////////////////////////////////////

    var command = ffmpeg()
    //select the input file
    .input(path.join(__dirname, 'upload', fileName+'.mp4'))
    //.input('/dev/video0')
    //read input at native framerate
    .inputOption('-re')
    //let the video in mp4 to be segmentated, in this way the video can be sent in pieces
    .outputOptions('-movflags frag_keyframe+empty_moov+faststart')
    // //good for fast encoding and low-latency streaming (ffmpeg documentation)
    .outputOptions('-tune zerolatency')
    //the fragment duration (indicatively every 0.04 a fragment is produced and then sent)
    .outputOptions('-frag_duration 4000')
    // //the video quality in vbr, the higher the value is, the lesser data are sent
    //.outputOptions('-crf '+crf)
    //set a target 2Mbps
    //.videoBitrate('200k', true)
    //stream the file without re-encoding
    .outputOptions('-codec copy')
    //the output format
    .format(format)
    //video encoding library
    //.videoCodec('libx264')
    //frame per second
    //.FPS(fps)
    //total duration
    //.setDuration(15)
    //.noAudio()
    //when the setDuration fires, this event is emitted

    if (srtp){
      command.outputOptions('-srtp_out_suite SRTP_AES128_CM_HMAC_SHA1_32')
      command.outputOptions('-srtp_out_params zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz')
    }

    command.output(res)

    command.on('end', function(){
      //console.log('streaming ended');
      setTimeout(function(){
        console.log('resolving')
        resolve()
      }, 2000)
    })
    command.on('err', function(err){
      if (err){
        console.log('error: '+err)
        reject('error: '+err)
      }
    })
    command.run()

  })
}
////////////////////////////////////////
//	Python script to extract the data
//  from the .csv file
////////////////////////////////////////

function extract(fileName, _protocol){

  return new Promise((resolve, reject)=>{

    let protocol
    switch (_protocol) {
      case "HTTPS":
        protocol = ['-pro', 'HTTPS']
        break;
      case "RTP":
        protocol = ['-pro', 'RTP']
        break;
      case "SRTP":
        protocol = ['-pro', 'SRTP']
        break;
      case "RTSP":
        protocol = ['-pro', 'RTSP'] 
        break;
      default:
        reject('Unsopported protocol')
    }

    var script = path.join(__dirname,'scripts')
    let options = {
      mode: "text",
      pythonOptions: ['-u'],
      scriptPath: script,
      args: ["-f", fileName, ...protocol]
    }

    PythonShell.run('extractor.py', options, function(err, results){
      if(err){
        reject("error while running the extractor python script. Error:\n"+err)
      }
      else{
        resolve()
      }
    })
  })
}

function deleteCAP(){
  return new Promise((resolve, reject)=>{
    glob(path.join(__dirname,'results', 'CAP*'), function(err, files){
      if(err)
        reject(err)
      else{
        let promises =  []
        files.forEach(file=>{
          promises.push(
            new Promise((resolve, reject)=>{
              fs.unlink(file, (err)=>{
                if (err)
                  reject('errore nella cancellazione dei CAP file: '+ err)
                else
                  resolve()
              })
            })
          )
        })
       Promise.all(promises)
       .then(resolve)
       .catch(reject)
      }
    })
  })
}

function wipeResults(){
  return new Promise((resolve, reject)=>{
    glob(path.join(__dirname,'results', '*'), function(err, files){
      if(err)
        reject(err)
      else{
        files.forEach(file=>{
          fs.unlink(file, (err)=>{
            if (err)
              console.log('errore nella cancellazione dei results: ' + err)
          })
        })
        glob(path.join(__dirname+'graphs','*'), function(err, files){
          if(err)
            reject(err)
          else{
            files.forEach(file=>{
              fs.unlink(file, (err)=>{
                if (err)
                  console.log('errore nella cancellazione dei grafici: ' + err)
              })
            })
            resolve()
          }
        })
      }
    })

  })
}

function deleteVideo(){
  return new Promise((resolve, reject)=>{
    glob(path.join(__dirname,'upload','*'), function(err, files){
      if(err)
        reject(err)
      else{
        files.forEach(file=>{
          fs.unlink(file, (err)=>{
            if (err)
              console.log('errore durante la cancellazione dei video: ' + err)
          })
        })
        resolve()
      }
    })
  })
}

////////////////////////////////////////
//	Setting up and starting the server
////////////////////////////////////////

https.createServer(options, app).listen(config.SERVERPORT);
console.log('listening on port: '+config.SERVERPORT)

app.use(cors(corsOptions))
// Serve the static files from the React app
//app.use(express.static(path.join(__dirname, 'client/build')));

app.use(bodyParser.json({limit: '500mb', extended: true})); // support json encoded bodies
app.use(bodyParser.urlencoded({limit: '500mb', extended: true })); // support encoded bodies


////////////////////////////////////////
//	Functions offered to clients
////////////////////////////////////////
app.get('/api/listener', function(req, res){
  var fileName = req.query.fileName
  console.log('iscritto al file '+fileName)
  myEmitter.once('ready'+fileName, ()=>{
    res.send('Ready to download')
    res.end()
    console.log(fileName+' pronto per il download')
  })
})

let starting = true;
app.post('/api/upload', async function(req, res) {
  ////////////////////////////////////////
  //	Wiping old results and videos
  ////////////////////////////////////////
  if(starting){
    await wipeResults()
    await deleteVideo()
    starting = false
  }

  var form = new IncomingForm()
  console.log('arrivata la richiesta')

  let dir = path.join(__dirname, 'upload')
  //create the folder if it does not exists
  if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
  }

  form.on('fileBegin', (name, file) => {
    file.path = path.join(__dirname, 'upload', file.name)
  })

  form.parse(req)

  form.on('end', ()=>{
    res.write('upload finished')
    res.end()
  })

})

app.get('/api/reset', function(req, res){
  console.log('resetting the environment')
  starting = true
  wipeResults()
  deleteVideo()
  res.send()
})

// app.get('/api/downloadCSV', function(req, res){
//   var file = __dirname + '/results/packageLength.csv';
//   res.download(file);
// });

// app.get('/api/downloadGraph', function(req, res){
//   var file = __dirname + '/results/graph.svg';
//   res.download(file);
// });

app.get('/api/downloadZip', function(req, res){
  deleteCAP()
  .then(()=>{
    let file = path.join(__dirname, 'results.zip')
    zipFolder(path.join(__dirname, 'results'), file, function(err) {
      if(err) {
        console.log('oh no!', err);
      } else {
        res.download(file);
      }
    });
  })
  .catch((err)=> {
    console.log('errore nel download dello zip file:\n'+err)
  })
});

app.get('/api/streamVideo',  function(req, res){
  var crf = req.query.crf
  var fps = req.query.fps
  var fileName = req.query.fileName
  var protocol = req.query.protocol

  console.log('start capturing')
  spawnTshark(fileName, protocol)
  .then(async ()=>{
    if(protocol == "RTSP")
      var RTSPServer = await spawnRTSPServer()
    console.log('starting streaming')
    stream(res, fps, crf, fileName, protocol)
    .then(()=>{
      //the package were saved in /server/results/cap.csv
      console.log('killing tshark')
      setTimeout(()=>{
        var promises = []
        
        promises.push(killProcess('ffmpeg'))
        promises.push(killProcess('tshark'))
        promises.push(killProcess('dumpcap'))
        

        Promise.all(promises)
        .then(()=>{
          //calling the python script to extract only the package size
          console.log('extracting packets\' length')
          extract(fileName, protocol)
          .then(()=>{
            //creating the graph
            // graphMaker.createGraph(fps, crf, protocol)
            // .then(()=>{
            myEmitter.emit('ready'+fileName)
            // })
            res.send('ok')
            res.end()

          })
          .catch((err)=>{
            console.log('errore nell\'extractor')
            console.log(err)
            myEmitter.emit('ready'+fileName)
          })
        })
        .catch((err)=>{
          console.log('errore nella kill process')
          console.log(err)
        })
      }, 2000)
    })
  })
  .catch((err)=>{
    console.log('error: '+err)
    res.send(err)
    res.end()
  })
 
})
