import sys
import os
import glob
import shutil
import re

SOURCE_FOLDER = None
DEST_FOLDER = None

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def parseInput(input):
  a = 0
  while(a<len(input)):
    if(input[a]=="-s"):
      a= a+1
      global SOURCE_FOLDER
      SOURCE_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), input[a])
      a= a+1
      continue
    if(input[a]=="-d"):
      a= a+1
      global DEST_FOLDER
      DEST_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)),input[a])
      a= a+1
      continue
    else:
      break
  if(SOURCE_FOLDER != None and DEST_FOLDER != None):
    return os.path.isdir(SOURCE_FOLDER) and os.path.isdir(DEST_FOLDER)
  else:
    return False
# except:
#   return False

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

### MAIN ###
if (parseInput(sys.argv[1:])):

  try:
    #get the list of all the files inside the source directory
    files = []
    for (dirpath, dirnames, filenames) in os.walk(SOURCE_FOLDER):
      files.extend(filenames)
      break
    #sorting strings with number inside, just to make the print fancy
    files.sort(key=natural_keys)
    #get the number of directories
    for file in files:
      #getting the directory name
      directory_name = str(file).split(".")[0].split("_")[1]
      #check if the derictory exists
      if(os.path.isdir(os.path.join(DEST_FOLDER, directory_name))):
        #save the file inside the folder
        print('saving {} '.format(directory_name))
        shutil.copy2(os.path.join(SOURCE_FOLDER,file), os.path.join(DEST_FOLDER, directory_name, "captured_size.txt"))
      if(os.path.isfile(os.path.join(DEST_FOLDER, directory_name, "size.txt"))):
        original_frame_number = sum(1 for line in open(os.path.join(DEST_FOLDER, directory_name, "size.txt")))
        captured_frame_number = sum(1 for line in open(os.path.join(DEST_FOLDER, directory_name, "captured_size.txt")))
        if (original_frame_number == captured_frame_number):
          print(bcolors.OKGREEN+"Same number of frame: {}".format(original_frame_number)+bcolors.ENDC)
        else:
          print(bcolors.FAIL+"Some frames have been lost, # of frames lost: {}".format(original_frame_number-captured_frame_number)+bcolors.ENDC)
      else:
        #printing a warning and skipping the file
        print(bcolors.WARNING+'directory {} not found, skipping the associated results file'.format(directory_name)+bcolors.ENDC)
        continue
    print('Done!')
  except Exception as error:
    print(bcolors.FAIL + str(error) + bcolors.ENDC)
else:
  print("\nError in the input.\nCheck if the syntax is correct and if the entered folders actually exists.\n\nThe correct syntax is:\nvideoGetter.py\n\t-s : path of the folder containing the results CSV\n\t-d : path where the iter folders are located")
  print("\nExample:\n\t distributeResults.py -s results/ -d video/")
#iterare tutte le cartelle e prendere i video.mp4, copiarli come nome_cartella.mp4 e spostarli in videoDir
