import matplotlib.pyplot as plt
import os
import pandas as pd
import numpy as np
from sklearn import preprocessing
import sklearn.ensemble as ens
import scipy.stats as stat1
import statistics  as stat2
import tsfresh as tsf
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

### data structure ###
'''
     array of tuples, with the folliwing format:
     [ID, label, ft1, ft2, .., ftn-1, ftn], ID will go from 1 to len(samples),
     following the name of the files "PL_iterXY.csv"
'''
#list of the extracted features
stats = [
    'MEAN',
    'GEOMETRIC_MEAN',
    'PVARIANCE',
    'SKEWNESS',
    'KURTOSIS',
    'QUANTILE_0.2',
    'QUANTILE_0.4',
    'QUANTILE_0.6',
    'QUANTILE_0.8',
    '1_SIGMA',
    '2_SIGMA'
]

#in the future getting the list of files in the whole directory
number_of_samples = range(1,409)
#array with the ID column and the label column
data = [[],[]]
#adding a column for each feature
for i in stats:
    data.append([])
    



samplePath = os.path.join('..','captured','capVBR-4DIR')
#samplePath = os.path.join('..','..','..','captured','prova')
#preparing prefix and extension of the files
filePrefix = 'PL_iter'
fileExt = '.csv'
fillZeroFlag = False

print("Extracting features...")
for i in number_of_samples:
    if i <10:
        fillZeroFlag = True
    else:
        fillZeroFlag = False
    
    if fillZeroFlag == True:
        fileName = (filePrefix+'0'+str(i)+fileExt)
    else:
        fileName = (filePrefix+str(i)+fileExt)
    
    #reading input data
    df = pd.read_csv(os.path.join(samplePath, fileName))
    ### pre-processing ###

    #appending the ID
    data[0].append(i)
    #appending the label, the video are orderd in rotating directions, that is: front(0), right(1), back(2), left(3->1)
    label = (i-1)%4
    #merging left and right
    if label==3:
        label = 1
    data[1].append(label)
    
    #removing outliers
    df = df[np.abs(df-df.mean()) <= (3.5*df.std())]
    df = df.dropna()
    #smoothing datas
    df = df.rolling(50).mean()
    df = df.dropna()
    
    #solo plot  +++ +++ +++  
    
    #scaling data, with min-max scaler
    #min_max_scaler = preprocessing.MinMaxScaler()
    #x_scaled = min_max_scaler.fit_transform(df.values)
    #df = pd.DataFrame(x_scaled)
   
    ### feature-extraction ###
    for stat in stats:
        aux =pd.to_numeric(df.iloc[:,0])
        if stat=='MEAN':
            data[2].append(stat2.mean(aux))
        if stat=='GEOMETRIC_MEAN':
            data[3].append(stat1.gmean(aux))
        if stat=='PVARIANCE':
            data[4].append(stat2.pvariance(aux))
        if stat=='SKEWNESS':
            data[5].append(stat1.skew(aux))
        if stat=='KURTOSIS':
            data[6].append(stat1.kurtosis(aux))
        if stat=='QUANTILE_0.2':
            data[7].append(np.quantile(aux, 0.2))
        if stat=='QUANTILE_0.4':
            data[8].append(np.quantile(aux, 0.4))
        if stat=='QUANTILE_0.6':
            data[9].append(np.quantile(aux, 0.6))
        if stat=='QUANTILE_0.8':
            data[10].append(np.quantile(aux, 0.8))
        if stat=='1_SIGMA':
            data[11].append(tsf.feature_extraction.feature_calculators.ratio_beyond_r_sigma(aux, 1))
        if stat=='2_SIGMA':
            data[12].append(tsf.feature_extraction.feature_calculators.ratio_beyond_r_sigma(aux, 2))

dict = {
    'ID': data[0],
    'label': data[1],
    'MEAN': data[2],
    'GEOMETRIC_MEAN': data[3],
    'PVARIANCE': data[4],
    'SKEWNESS': data[5],
    'KURTOSIS': data[6],
    'QUANTILE_0.2': data[7],
    'QUANTILE_0.4': data[8],
    'QUANTILE_0.6': data[9],
    'QUANTILE_0.8': data[10],
    '1_SIGMA': data[11],
    '2_SIGMA': data[12]
}

df = pd.DataFrame(data=dict)
print("Splitting the dataset")
#splitting training
X_train, X_test, y_train, y_test = train_test_split(df.iloc[:, 2:], df['label'], test_size=0.66, random_state=42)


tree=ens.RandomForestClassifier(n_estimators=100, max_features=0.3, n_jobs=-1)
print("Training the model")
tree.fit(X_train, y_train) 
print("Predicting the sample")
y_pred=tree.predict(X_test)

print(classification_report(y_test, y_pred)+'\n')
print("LABELS PREDICTED:\n"+str(list(y_pred))+'\n')
print("TRUE LABELS:\n"+str(list(y_test)))

#plt.figure(figsize=(20,10))
#plt.plot(range(0, len(df)), df, linewidth=1)
#plt.show()

   