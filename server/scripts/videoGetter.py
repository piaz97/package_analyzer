#prendere in input il path in cui sono presenti le cartelle iter, il path di destinazione
import sys
import os
import glob
import shutil

SOURCE_FOLDER = None
DEST_FOLDER = None

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def parseInput(input):
  a = 0
  while(a<len(input)):
    if(input[a]=="-s"):
      a= a+1
      global SOURCE_FOLDER
      SOURCE_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), input[a])
      a= a+1
      continue
    if(input[a]=="-d"):
      a= a+1
      global DEST_FOLDER
      DEST_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)),input[a])
      a= a+1
      continue
    else:
      break
  if(SOURCE_FOLDER != None and DEST_FOLDER != None):
    return os.path.isdir(SOURCE_FOLDER) and os.path.isdir(DEST_FOLDER)
  else:
    return False
# except:
#   return False

### MAIN ###
if (parseInput(sys.argv[1:])):

  try:
    #get the list of all the directory inside the source directory
    directories = []
    for (dirpath, dirnames, filenames) in os.walk(SOURCE_FOLDER):
      directories.extend(dirnames)
      break
    #get the number of directories
    for directory in directories:
      #check if the video exists
      video = []
      video.extend(glob.glob(os.path.join(SOURCE_FOLDER, directory, "*.mp4")))
      if(len(video)!=1):
        raise Exception('In the directory {}, there are {} video.\nEvery folder must contains one and only one video'.format(directory, len(video)))
      else:
        print('copying {}'.format(video[0]))
        shutil.copy2(video[0], os.path.join(DEST_FOLDER, directory+".mp4"))
    print('Done!')
  except Exception as error:
    print(bcolors.FAIL + str(error) + bcolors.ENDC)
else:
  print("\nError in the input.\nCheck if the syntax is correct and if the entered folders actually exists.\n\nThe correct syntax is:\nvideoGetter.py\n\t-s : path of the folder containg iter folders\n\t-d : path where to save the results")
  print("\nExample:\n\t videoGetter.py -s video/ -d results/")
#iterare tutte le cartelle e prendere i video.mp4, copiarli come nome_cartella.mp4 e spostarli in videoDir
