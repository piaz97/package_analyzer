import pandas as pd
import statistics
import sys
import os
import numpy as np
import codecs
###########################
# Constants
###########################

PROTOCOL = None
FILENAME = None
#PORT = None

###########################
# Input parser
###########################

def parseInput(input):
  #iterator
  try:
    a = 0
    while(a<len(input)):
      if(input[a]=="-fileName" or input[a]=="-f"):
        print("setting FILENAME")
        a= a+1
        global FILENAME
        FILENAME = input[a]
        a= a+1
        continue

      if(input[a]=="-protocol" or input[a]=="-pro"):
        print("setting PROTOCOL")
        a= a+1
        global PROTOCOL
        PROTOCOL = input[a]
        a= a+1
        continue
      else:
        break
    return True
  except:
    return False

def getTimeFlag(time):
  return int(time*100)

#############################################################
# Group packets by frame end (Packet size and arrival time) #
#############################################################

def rtp(df):
  #accumulator
  acc = 0
  index = 0

  time = df["_ws.col.Time"].values
  length = df["frame.len"].values
  info = df["_ws.col.Info"].values

  FLAG = max(set(length), key=length.tolist().count)

  timeFlag = getTimeFlag(time[0])
  #array containing the results
  ris = []
  while(index < len(time)):
    #if it is the end of a frame
    print(str(len(ris))+', pacchetto: '+str(index))
    if(PROTOCOL=="RTSP"):
      index += 1
      continue
    if(PROTOCOL=="SRTP" and ("Destination unreachable" in info[index])):
      index += 1
      continue

    # if((getTimeFlag(time[index]) > timeFlag+1) and acc!=0 and index!=0):
    #   timeFlag = getTimeFlag(time[index])
    #   #insert the acc (old frame)
    #   ris.append(acc)
    #   #update the accumulator with the new frame start
    #   acc = length[index]
    #   acc -=54
    # else:
    #   acc+=length[index]
    #   acc-=54
    acc+= length[index]-54
    if("Mark" in info[index]):
      # if(index+1!=len(length)):
      #   timeFlag = getTimeFlag(time[index+1])
      ris.append(acc)
      acc = 0
    index += 1
  if (acc!=0):
    ris.append(acc)
  return pd.DataFrame(ris)

def occurrencies(find, sentence):
  words = str(sentence).split(" ")
  counter = 0
  for word in words:
    if(find in word):
      counter+=1
  return counter



def preProcessHTTPS(df):

  acc = 0
  index = 0

  time = df["_ws.col.Time"].values
  length = df["frame.len"].values
  info = df["_ws.col.Info"].values
  source = df["_ws.col.Source"].values

  #setting the flag related to the first frame arrival
  lastTimestamp = getTimeFlag(time[index])

  #array containing the results
  ris = []

  while(index < len(time)):
    #if it is a packet sent by the server (video frame)
    if(source[index]!=443):
      #check if the second decimal is changed
      currentTimestamp = getTimeFlag(time[index])
      if(lastTimestamp-1<=currentTimestamp<= lastTimestamp+1):
        #if not, the frame is the same
        acc += length[index]
        #removing the const header of eth/ip/tcp protocols
        acc -= 66
        #removing the tls fragment headers (5 bytes, type(1), version(2) and length(2))
        acc-= (5*occurrencies("Application", info[index]))
      else:
        #otherwise, save the accumulated size and reset the accumulator
        ris.append(acc)
        acc = length[index]
        #update the frameTimestamp
        lastTimestamp = currentTimestamp
        print('new timestamp: '+str(lastTimestamp))
    index += 1

  return pd.DataFrame(ris)


### MAIN ###
print(sys.argv[1:])
if (parseInput(sys.argv[1:])):
  filePath= os.path.join(os.path.dirname(__file__),'..','results','CAP_'+FILENAME+'.csv')

  lines = []
  with codecs.open(filePath, 'r', errors='ignore') as readFile:
    lines = readFile.readlines()
  
  begin = False
  with codecs.open(filePath, 'w') as writeFile:
    for line in lines:
      if(begin):
        writeFile.write(line)
      else:
        if (line.startswith("frame")):
          writeFile.write(line)
          begin = True

  df = pd.read_csv(filePath)
  df = df.sort_values('_ws.col.Time')
  if (PROTOCOL == "RTP" or PROTOCOL == "SRTP" or PROTOCOL == "RTSP"):
    df = rtp(df)
  if (PROTOCOL == "HTTPS"):
    df = preProcessHTTPS(df)
    #df = df["frame.len"]

  #df = df[np.abs(df["frame.len"]-df["frame.len"].mean()) <= (3.5*df["frame.len"].std())]
  print(df)
  df.to_csv(os.path.join(os.path.dirname(__file__),'..','results','PL_'+FILENAME+'.csv'), header=False, index=False)
  print('Done!')
else:
  print('error')
