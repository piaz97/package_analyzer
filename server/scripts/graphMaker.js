////////////////////////////////////////
//	Plotly configuration constants
////////////////////////////////////////

const api_key = 'tntzU1U8b5XepllUORHf'
const username = 'piaz97'

////////////////////////////////////////
var fs = require('fs')
var plotly = require('plotly')(username, api_key)
const glob = require('glob')

module.exports = {
  createGraph: function(fps=25, crf=23, protocol='HTTPS'){
    return new Promise((resolve, reject)=>{
      let dir = __dirname+'/../server/graphs';
      if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
      }
      glob(__dirname+'/../server/results/PL*', function(err, files){

        if(err)
          console.log(err)
        else{
          files.forEach(file=>{
            var csvPath = file
            var packageLength = fs.readFileSync(csvPath)
              .toString()
              .split('\n')
              .map(e => parseInt(e.trim()))

            var indexes = [...Array(packageLength.length).keys()]

            var trace = [
              {
                x: indexes,
                y: packageLength,
                type: "line"
              }
            ];

            var layout = {
              title: "Dimensione dei pacchetti | FPS: "+fps+" | CRF: "+crf+" | protocollo: "+protocol,
              xaxis: {
                title: 'Packet\'s number'
              },
              yaxis: {
                title: 'Packet\'s size (bytes)'
              }
            };

            var figure = {
              data: trace,
              layout: layout
            };

            var imgOpts = {
              format: 'png',
              height: '900',
              width: '1440'
            };
            let fileName = file.split('/').slice(-1).join('').split('_').slice(-1).join('').split('.').slice(0,1)
            var destFilePath = __dirname+'/../server/graphs/GR_'+fileName+'.png'

            plotly.getImage(figure, imgOpts, function (error, imageStream) {
              if (error)
                return console.log (error);

              var fileStream = fs.createWriteStream(destFilePath);
              imageStream.pipe(fileStream);
            })
            console.log('creo grafico')
          })
          resolve()
        }
      })
    })

  }
}



