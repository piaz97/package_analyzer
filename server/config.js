////////////////////////////////////////
//	OPTIONS FOR THE SERVER
////////////////////////////////////////

module.exports = {
  PROTOCOL: 'https',
  SERVERPORT: 443,
  DOMAIN: "localhost",
  CLIENTPORT: 20000
}
