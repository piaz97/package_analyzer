const initialState = {
  file: null,
  streaming: false,
  results: false,
  uploaded: false,
  completed: 0,
  protocol: 'HTTPS'
};

export function rootReducer(state = initialState, action) {
  if (action.type === "ADDFILE") {
    return Object.assign({}, state, {
      file: action.file
    })
  }

  if (action.type === "UPLOADED"){
    return Object.assign({}, state, {
      uploaded: true
    })
  }
  if (action.type === "BEGINSTREAMING") {
    return Object.assign({}, state, {
      streaming: true
    })
  }

  if (action.type === "RESULTS") {
    return Object.assign({}, state, {
      results: true
    })
  }

  if (action.type === "RESET") {
    return Object.assign({}, state, {
      ...initialState
    })
  }

  if (action.type === "INCREASECOMPLETED") {
    return Object.assign({}, state, {
      completed: state.completed+1
    })
  }


  if (action.type === "SETPROTOCOL") {
    return Object.assign({}, state, {
      protocol: action.protocol
    })
  }

  //returning the state
  return state;
}
