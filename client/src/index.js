import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';
import './style/index.css';
import {store} from './store/store'
import * as serviceWorker from './serviceWorker';
import AppContainer from './components/containers/AppContainer';
import history from './store/history';

ReactDOM.render(
    <Provider store={store}>
      <Router history={history}>
          <Route path="/" component={AppContainer} />
      </Router>
    </Provider>
    ,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
