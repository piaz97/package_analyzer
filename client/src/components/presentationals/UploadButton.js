import React, { Component } from "react";
import config from '../../config'

var server = config.PROTOCOL+'://'+config.DOMAIN+':'+config.SERVERPORT
class UploadButton extends Component{

  constructor(props){
    super(props)
    this.handleAnalyze = this.handleAnalyze.bind(this)
    this.handleStreaming = props.handleStreaming.bind(this)
    this.handleResults = props.handleResults.bind(this)
    this.handleUpload = props.handleUpload.bind(this)
  }


  //sending the file to express API
  sendRequest(files) {
    if(files!==null && files!==undefined){
      files = Array.from(files)
      return new Promise((resolve, reject)=>{
        var promises= []
        files.forEach(file => {
          promises.push(
            new Promise((resolve, reject) => {
              const formData = new FormData();
              formData.append("file", file, file.name);
              const req = new XMLHttpRequest();
            // return new Promise((resolve, reject) => {
              req.open("POST", server+"/api/upload", false);
              req.send(formData);
              if (req.status === 200) {
                resolve(req.responseText);
              }
              else{
                reject("errore nell'upload al server")
              }
            })
          )
        });
        Promise.all(promises)
        .then(()=>{
          resolve()
        })
        .catch((err)=>{
          reject(err)
        })
      })
    }
  }

  handleAnalyze() {
    if(this.props.file!==undefined && this.props.file !==null){
      this.sendRequest(this.props.file)
      .then(()=>{
        this.handleUpload();
      })
      .catch((err)=>{
        console.log('errore nell\'upload: '+err)
      })
    }
    else
      console.log("inserire almeno un file prima di cominciare l'analisi")
  }

  render(){
    return (
        !this.props.uploaded ?
          (
            <button className='Button MarginTop' onClick={()=>{this.handleAnalyze()}}>
                UPLOAD
                <i className="material-icons ">
                  backup
                </i>
            </button>
          )
        :
          (
            <button className='Button MarginTop' onClick={()=>{this.handleStreaming()}}>
              ANALYZE
              <i className="material-icons ">
                search
              </i>
            </button>
          )
    )
  }
}

export default UploadButton;


