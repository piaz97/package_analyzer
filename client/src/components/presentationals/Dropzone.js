import React, { Component } from "react";
import '../../style/Dropzone.css'
import cloud from '../../style/img/upload.svg'
import correct from '../../style/img/correct.svg'
import list from '../../style/img/list.svg'
class Dropzone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hightlight: false,
    };
    this.fileInputRef = React.createRef();

    this.openFileDialog = this.openFileDialog.bind(this);
    this.onFilesAdded = this.onFilesAdded.bind(this);
    this.onDragOver = this.onDragOver.bind(this);
    this.onDragLeave = this.onDragLeave.bind(this);
    this.onDrop = this.onDrop.bind(this);
  }

  fileUpload(files) {
    this.props.handleUpload(files)
  }

  openFileDialog() {
    if (this.props.disabled) return;
    this.fileInputRef.current.click();
  }

  onFilesAdded(evt) {
    if (this.props.disabled) return;
    const files = evt.target.files;
    if (this.props.onFilesAdded) {
      const array = this.fileListToArray(files);
      this.props.onFilesAdded(array);
    }
    this.fileUpload(files)
  }

  onDragOver(evt) {
    evt.preventDefault();
    if (this.props.disabled) return;

    this.setState({ hightlight: true });
  }

  onDragLeave(event) {
    this.setState({
      hightlight: false
    });

  }

  onDrop(event) {
    event.preventDefault();

    if (this.props.disabled) return;

    const files = event.dataTransfer.files;
    if (this.props.onFilesAdded) {
      const array = this.fileListToArray(files);
      this.props.onFilesAdded(array);
    }
  }

  fileListToArray(list) {
    const array = [];
    for (var i = 0; i < list.length; i++) {
      array.push(list.item(i));
    }
    return array;
  }

  render() {
    return (
      <div
        className='Dropzone'
        onDragOver={this.onDragOver}
        onDragLeave={this.onDragLeave}
        onDrop={this.onDrop}
        onClick={this.openFileDialog}
        style={{ cursor: this.props.disabled ? "default" : "pointer" }}
      >
        <input
          ref={this.fileInputRef}
          className="FileInput"
          type="file"
          multiple
          onChange={this.onFilesAdded}
        />
        <img
          alt="upload"
          className="Icon"
          src={this.props.file===null? cloud: (this.props.uploaded ? correct : list)}
        />
        <span className="MarginTop" >{this.props.file===null? "Upload Video": (this.props.uploaded ? "Video uploaded" : "Video selected") }</span>
      </div>
    );
  }
}

export default Dropzone;
