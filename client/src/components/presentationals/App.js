import React, {Component} from 'react';
import '../../style/App.css';
import 'material-design-icons/iconfont/material-icons.css';

import InputGroup from 'react-bootstrap/InputGroup';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';
import Card from 'react-bootstrap/Card';
import ProgressBar from 'react-bootstrap/ProgressBar';
import Spinner from 'react-bootstrap/Spinner'
import DropzoneContainer from '../containers/DropzoneContainer';
import UploadButtonContainer from '../containers/UploadButtonContainer';

import zip from "../../style/img/zip.svg"
import config from '../../config';
import util from "../../util/utils"

var server = config.PROTOCOL+'://'+config.DOMAIN+':'+config.SERVERPORT


class App extends Component {

  constructor(props) {
    super(props)
    this.state = ({
      fps: undefined,
      crf: undefined,
      outliers: false
    })
    this.handleChange = this.handleChange.bind(this);
    this.handleReset = props.handleReset.bind(this);
    this.handleIncrease = props.handleIncrease.bind(this);
    this.handleProtocol = props.handleProtocol.bind(this);
    this.changeProtocol = this.changeProtocol.bind(this);
  }

  handleChange(e) {
    this.setState({
        ...this.state,
        [e.target.name]: e.target.value
    });
  }

  changeProtocol(e) {
    this.handleProtocol(e.target.value)
  }
  handleToggle(e) {
    this.setState({
      ...this.state,
      outliers: !this.state.outliers
    })
  }

  handleListener(fileName){
    return new Promise((resolve, reject)=>{
      const req = new XMLHttpRequest();
      const handleIncrease = this.handleIncrease;
      console.log('listening file: '+fileName)
      req.open("GET", server+"/api/listener?fileName="+fileName, true);
      req.send()
      resolve()
      req.onload = function (e) {
        if (req.status === 200) {
          console.log('stop listening file: '+fileName)
          handleIncrease()
        }
      }
    })
  }

  render(){
    var header;
    var status;
    // if (this.props.results) {
    //   status = (
    //     <div className= "flexContainerRow">
    //       <a href={server+'/api/downloadGraph'}>
    //         <img
    //         alt="graph"
    //         className="bigIcon"
    //         src={graph}
    //         />
    //       </a>
    //       <a href={server+'/api/downloadCSV'}>
    //         <img
    //         alt="csv"
    //         className="bigIcon"
    //         src={csv}
    //         />
    //       </a>
    //     </div>
    //   )
    // }
    // else {

    //completion bar che si aggiorna ad ogni nuovo video completato

    if(this.props.streaming) {
      var numberOfFiles = Array.from(this.props.file).length
      status = (
        <ProgressBar
          animated
          striped
          variant="info"
          now={(this.props.completed/numberOfFiles)*100}
          label={"ANALYZING "+this.props.completed+'/'+numberOfFiles}
          className= "allWidth"
        />
      )
      //se completed è uguale a numberOfFiles => chiedi i risultati
      if (this.props.completed === numberOfFiles){
        header = (
          <div className= "flexContainerRow">
            <a href={server+'/api/downloadZip'}>
              <img
                alt="download zip"
                className="bigIcon"
                src={zip}
              />
            </a>
          </div>
        )
      }
      else{
        //altrimenti richiedi il video in posizione completed

        //monitoring the result with the listener


        let currentFile = Array.from(this.props.file)[this.props.completed]

        this.handleListener(util.removeFormat(currentFile.name))
        .then(()=>{
          var streamingPath = '/api/streamVideo?fileName='+util.removeFormat(currentFile.name)
          if(this.state.fps!==undefined)
            streamingPath+='&fps='+this.state.fps
          if(this.state.crf!==undefined)
            streamingPath+='&crf='+this.state.crf
          streamingPath+='&protocol='+this.props.protocol
  
          const stream = new XMLHttpRequest();
          stream.open("GET", server+streamingPath, true);
          stream.send()
        })

        header = (

          <div className="flexContainerColumn">
            <Spinner animation="border" variant="primary" className="spinner" />
            {status}
          </div>
        )
      }
    }
    else{
      //se non sono in streaming allora deve esserci la possibilità di uploadare i file
      header = (
        <DropzoneContainer />
      )
    }

    return (
      <div className="App">
        <Card className="Card">
          <Card.Header className="flexContainerCol">
            {header}
          </Card.Header>
          <Card.Body className="flexContainerCol">
            <div className="flexContainerRow">
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text>
                    FPS
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="number"
                  max="60"
                  min="1"
                  placeholder="25"
                  aria-label="FPS"
                  id="fps"
                  name="fps"
                  onChange = {this.handleChange}
                  required
                />
              </InputGroup>

              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text >
                    crf
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="number"
                  max="51"
                  min="0"
                  placeholder="23"
                  aria-label="Constant Rate Factor"
                  id="crf"
                  name="crf"
                  onChange = {this.handleChange}
                  required
                />
              </InputGroup>
            </div>
            <ButtonGroup className="mr-2" aria-label="First group">
                <Button value='HTTPS' className={this.props.protocol==='HTTPS'?'groupSelected':'group'} onClick={this.changeProtocol} >HTTPS</Button>
                <Button value='RTP' className={this.props.protocol==='RTP'? 'groupSelected': 'group'} onClick={this.changeProtocol} >RTP</Button>
                <Button value='SRTP' className={this.props.protocol==='SRTP'? 'groupSelected': 'group'} onClick={this.changeProtocol} >SRTP</Button>
                <Button value='RTSP' className={this.props.protocol==='RTSP'? 'groupSelected': 'group'} onClick={this.changeProtocol} >RTSP</Button>
              </ButtonGroup>
            <UploadButtonContainer />

            <button className='Button buttonDanger MarginTop danger' onClick={()=>{this.handleReset()}}>
              RESET
              <i className="material-icons ">
                autorenew
              </i>
            </button>
          </Card.Body>
        </Card>
      </div>
    );
  }

}

export default App;
