import { connect } from 'react-redux';
import Dropzone from '../presentationals/Dropzone'

const mapDispatchToProps = (dispatch) => {
  return {
    handleUpload: function(file){
      console.log(file)
      dispatch({
        type: "ADDFILE",
        file: file
      })
    }
  }
}

const mapStateToProps = (state) => {
  return {
    file: state.file,
    uploaded: state.uploaded
  }
}

const DropzoneContainer = connect(mapStateToProps, mapDispatchToProps)(Dropzone);

export default DropzoneContainer;
