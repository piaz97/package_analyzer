import { connect } from 'react-redux';
import App from '../presentationals/App'
import history from '../../store/history';
import config from "../../config"

var server = config.PROTOCOL+'://'+config.DOMAIN+':'+config.SERVERPORT

const mapDispatchToProps = (dispatch) => {
  return {
    handleReset: function(){
      dispatch({
        type: "RESET"
      })
      const req = new XMLHttpRequest();
      req.open("GET", server+"/api/reset");
      req.send()
      history.push('/')
    },
    handleIncrease: function(){
      let action = {
        type: "INCREASECOMPLETED"
      }
      console.log('increasing')
      dispatch(action)

    },

    handleProtocol: function(protocol){
      let action = {
        type: "SETPROTOCOL",
        protocol
      }
      dispatch(action)
    }
  }
}

const mapStateToProps = (state) => {
  return {
    streaming: state.streaming,
    results: state.results,
    file: state.file,
    completed: state.completed,
    protocol: state.protocol
  }
}

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;
