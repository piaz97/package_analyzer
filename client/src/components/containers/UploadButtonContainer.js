import { connect } from 'react-redux';
import UploadButton from '../presentationals/UploadButton'
/**
 * @description map the editProduct action into the Button component
 * @param {*} dispatch
 * @param {*} ownProps
 */
const mapDispatchToProps = (dispatch) =>{

  return {
    handleStreaming: function(){
      let action = {
        type: "BEGINSTREAMING",
      }
      dispatch(action)
    },

    handleResults: function(){
      let action = {
        type: "RESULTS"
      }

      dispatch(action)
    },

    handleUpload: function(){
      let action = {
        type: "UPLOADED"
      }
      dispatch(action)
    }
  }
}

const mapStateToProps = (state) => {
  return {
    file: state.file,
    uploaded: state.uploaded
  }
}

const UploadButtonContainer = connect(mapStateToProps, mapDispatchToProps)(UploadButton);

export default UploadButtonContainer;
