var util = (function() {
  return {
    removeFormat: function(fileName){
     return fileName.toString().split(".").slice(0, -1).join('')
    }
  };
}());

export default util;
